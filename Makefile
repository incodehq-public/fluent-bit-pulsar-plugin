all:
	go build -buildmode=c-shared -o out_pulsar.so .

fast:
	go build out_pulsar.go

clean:
	rm -rf *.so *.h *~
