package main

import (
	"context"
	"fmt"
	"log"
	"unsafe"

	"C"

	"github.com/Terry-Mao/goconf"
	"github.com/apache/pulsar-client-go/pulsar"
	"github.com/fluent/fluent-bit-go/output"
	jsoniter "github.com/json-iterator/go"
)

func parseMap(mapInterface map[interface{}]interface{}) map[string]interface{} {
	m := make(map[string]interface{})
	for k, v := range mapInterface {
		switch t := v.(type) {
		case []byte:
			// prevent encoding to base64
			m[k.(string)] = string(t)
		case map[interface{}]interface{}:
			m[k.(string)] = parseMap(t)
		default:
			m[k.(string)] = v
		}
	}
	return m
}

func getValue(i map[interface{}]interface{}) ([]byte, error) {
	o := parseMap(i)

	//I'll be back
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	m, err := json.Marshal(&o)
	if err != nil {
		return nil, err
	}
	return m, nil
}

var (
	client   pulsar.Client
	producer pulsar.Producer
)

func initPulsar() {
	var err error
	var url string
	var topic string
	var token string

	conf := goconf.New()
	if err = conf.Parse("/fluent-bit/etc/pulsar.conf"); err != nil {
	}
	confpulsar := conf.Get("OUTPUT")
	if confpulsar == nil {
		url = "pulsar://localhost:6650"
		topic = "fluent-bit"
	}
	url, err = confpulsar.String("URL")
	if err != nil {
		url = "pulsar://localhost:6650"
	}
	topic, err = confpulsar.String("Topic")
	if err != nil {
		topic = "fluent-bit"
	}
	token, err = confpulsar.String("Token")
	if err != nil {
		token = ""
	}

	// Instantiate a Pulsar client
	client, err = pulsar.NewClient(pulsar.ClientOptions{
		URL:            url,
		Authentication: pulsar.NewAuthenticationToken(token),
	})

	if err != nil {
		fmt.Println("Instantiate Pulsar client failed:", err)
	}

	// Use the client to instantiate a producer
	producer, err = client.CreateProducer(pulsar.ProducerOptions{
		Topic: topic,
	})

	if err != nil {
		fmt.Println("instantiate Pulsar producer failed:", err)
	}
}

func deinitPulsar() {
	if producer != nil {
		producer.Close()
	}

	if client != nil {
		client.Close()
	}
}

//export FLBPluginRegister
func FLBPluginRegister(ctx unsafe.Pointer) int {
	return output.FLBPluginRegister(ctx, "pulsar", "Pulsar GO!")
}

//export FLBPluginInit
// (fluentbit will call this)
// ctx (context) pointer to fluentbit context (state/ c code)
func FLBPluginInit(ctx unsafe.Pointer) int {
	// Example to retrieve an optional configuration parameter
	param := output.FLBPluginConfigKey(ctx, "param")
	fmt.Printf("[flb-go] plugin parameter = '%s'\n", param)

	initPulsar()

	return output.FLB_OK
}

//export FLBPluginFlush
func FLBPluginFlush(data unsafe.Pointer, length C.int) int {
	var ret int
	var record map[interface{}]interface{}

	// Create Fluent Bit decoder
	dec := output.NewDecoder(data, int(length))

	// Iterate Records
	for {
		// Extract Record
		ret, _, record = output.GetRecord(dec)
		if ret != 0 {
			break
		}
		// Print record keys and values
		m, err := getValue(record)
		if err != nil {
			log.Printf("encoding/json: %s\n", err)
		} else {
			if producer != nil {
				// Attempt to send the message
				_, err = producer.Send(context.Background(), &pulsar.ProducerMessage{
					Payload: m,
				})
				if err != nil {
					fmt.Println("Pulsar send failed:", err)
				}
			}
		}
	}

	// Return options:
	//
	// output.FLB_OK    = data have been processed.
	// output.FLB_ERROR = unrecoverable error, do not try this again.
	// output.FLB_RETRY = retry to flush later.
	return output.FLB_OK
}

//export FLBPluginExit
func FLBPluginExit() int {
	deinitPulsar()
	return output.FLB_OK
}

func main() {
}
