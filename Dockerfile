FROM golang:1.15.3 as build
COPY . /src/pulsar
WORKDIR /src/pulsar

RUN make clean && make all

FROM fluent/fluent-bit:1.6.3
COPY --from=build /src/pulsar/out_pulsar.so /fluent-bit/bin
COPY fluent-bit.conf /fluent-bit/etc/fluent-bit.conf
EXPOSE 2020

CMD ["/fluent-bit/bin/fluent-bit", "-e","/fluent-bit/bin/out_pulsar.so", "-c", "/fluent-bit/etc/fluent-bit.conf"]