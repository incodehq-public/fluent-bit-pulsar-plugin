# Example: out_gpulsar

The following example code implements a simple output plugin that forward the records to Pulsar.

## Pulsar Configuration File

Pulsar configuration should be placed at /fluent-bit/etc/pulsar.conf. Sample config is shown below.

```
[OUTPUT]
    URL    pulsar://localhost:6650
    Topic  fluent-bit
    Token  eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJKb2UifQ.ipevRNuRP6HflG8cFKnmUPtypruRC4fb1DWtoLL62SY
```
