module gitlab.com/incodehq-public/fluent-bit-pulsar-plugin

go 1.15

require (
	github.com/Terry-Mao/goconf v0.0.0-20161115082538-13cb73d70c44
	github.com/apache/pulsar-client-go v0.2.0
	github.com/fluent/fluent-bit-go v0.0.0-20200729034236-b9c0d6a20853
	github.com/json-iterator/go v1.1.10
)
